#!/bin/bash
sed 's/^/export /g' ./config/envlocal.env > ./config/.profile
cp ./config/server.air.toml ./server.air.toml
source ./config/.profile && air -c server.air.toml
rm ./server.air.toml
rm ./config/.profile