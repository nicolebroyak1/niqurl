#!/bin/bash
sed 's/^/export /g' ./config/envlocal.env > ./config/.profile
source ./config/.profile && redis-server --port $REDIS_PORT --appendonly yes
rm ./config/.profile