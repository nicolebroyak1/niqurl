#!/bin/bash
sed 's/^/export /g' ./config/envlocal.env > ./config/.profile
cp config/cli.air.toml ./cli.air.toml
source ./config/.profile && air -c cli.air.toml
rm ./cli.air.toml
rm ./config/.profile