package main

import "gitlab.com/nicolebroyak1/niqurl/internal/app/cli"

func main() {
	cli.Start()
}
