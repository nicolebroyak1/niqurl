package main

import (
	"path"

	"github.com/gin-gonic/gin"

	"gitlab.com/nicolebroyak1/niqurl/config"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/api"
)

func main() {
	StartServer()
}

func StartServer() {
	server := gin.Default()
	tmplPath := path.Join(config.Configs["APP_PATH"], "internal", "app", "api", "templates")
	server.LoadHTMLFiles(
		path.Join(tmplPath, "404.html"),
		path.Join(tmplPath, "inspecturl.html"),
	)
	server.GET("/!:url", api.ShowURLInfo)
	server.GET("/:url", api.RedirectURL)
	Configs := config.Configs
	server.Run(":" + Configs["SERVER_PORT"])
}
