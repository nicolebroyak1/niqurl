package redishandler

import "fmt"

func isValidSetting(setting string) bool {
	_, err := Client.Get(Context, setting).Result()
	return err == nil
}

func existsValueAsHashSet(hashSetName, value string) bool {
	hash := fmt.Sprintf("%v:%v", hashSetName, value)
	booleanInt := Client.Exists(Context, hash).Val()
	return booleanInt != 0
}

func ExistsShortURL(shortURL string) bool {
	return existsValueAsHashSet("url", shortURL)
}

func ExistsLongURL(longURL string) bool {
	return existsValueAsHashSet("url", longURL)
}

func existsUser(username string) bool {
	return existsValueAsHashSet("user", username)
}

func IsUserOnWaitTime(user string) bool {
	isUserLimited, _ := Client.Get(Context, user).Bool()
	return isUserLimited
}
