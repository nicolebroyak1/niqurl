package redishandler

import (
	"log"

	"gitlab.com/nicolebroyak1/niqurl/config"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/randomusers"
)

// used regularly in CLI to constantly provide valid settings
func SetInvalidSettingsToDefaults() {
	for setting, defaultValue := range config.Configs {
		if !isValidSetting(setting) {
			ChangeSetting(setting, defaultValue)
		}
	}
	if GetStringSetting("USER_COUNT") == "0" {
		apiSource := GetStringSetting("DEFAULT_API_SOURCE")
		apiSource = randomusers.CreateAPISourceFromDefault(apiSource, 5)
		userStruct := randomusers.GenerateFakeUsers(apiSource, 5)
		InsertUsers(userStruct)
	}
}

func ChangeSetting(setting string, value string) {
	Client.Set(Context, setting, value, 0)
	log.Printf("%v set to %v\n", setting, value)
}

func incrementUsersCount() {
	Client.Incr(Context, "USER_COUNT")
}

func incrementURLCount() {
	Client.Incr(Context, "URL_COUNT")
}
