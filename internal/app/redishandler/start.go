package redishandler

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/nicolebroyak1/niqurl/config"
)

func Start() *redis.Client {
	Configs := config.Configs
	client := redis.NewClient(&redis.Options{
		Addr:     Configs["REDIS_PATH"],
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	return client
}
