package redishandler

import (
	"testing"

	"fmt"
)

func init() {
	SetInvalidSettingsToDefaults()
}

func TestChangeSetting(t *testing.T) {
	urlLenOld, err := Client.Get(Context, "SHORT_URL_LEN").Int()
	if err != nil {
		t.Fatalf("Error from redis Client")
	}
	urlLenNew := fmt.Sprintf("%v", urlLenOld+2)
	ChangeSetting("SHORT_URL_LEN", urlLenNew)
	urlLenNewInt, err := Client.Get(Context, "SHORT_URL_LEN").Int()
	Client.Set(Context, "SHORT_URL_LEN", urlLenOld, 0)
	if err != nil {
		t.Fatalf("Error from redis Client")
	}
	if urlLenNewInt != urlLenOld+2 {
		t.Fatalf(`Error: %v want match for %v`, urlLenNew, urlLenOld+2)
	}
}
