package redishandler

import (
	"fmt"
	"log"
	"path"
)

func printExistingShortURL(shortURL string) {
	fmt.Print("URL shortened before to: ")
	fmt.Println(path.Join(GetStringSetting("SERVER_PATH"), shortURL))
}

func printInsertingURLMsg(longURLstring, shorturl string) {
	fmt.Print("Creating short URL for")
	fmt.Printf(" [ %v ]: ", longURLstring)
	fmt.Println(path.Join(GetStringSetting("SERVER_PATH"), shorturl))
}

func PrintUserWaitTime(user string) {
	waittime := Client.TTL(Context, user).Val()
	log.Printf("User %v has to wait %v ms to shorten url again", user, waittime)
}

func PrintCurrentCLISettings() {
	fmt.Println("Current settings")
	fmt.Printf("short url length: %q characters\n", GetStringSetting("SHORT_URL_LEN"))
	fmt.Printf("user wait time: %q ms \n", GetStringSetting("USER_WAIT_TIME"))
}
