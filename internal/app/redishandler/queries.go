package redishandler

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"
)

// function assumes that setting was validated before invoking
func GetSetting(setting string) int {
	settingValue, _ := Client.Get(Context, setting).Int()
	return settingValue
}

func GetStringSetting(setting string) string {
	settingValue, _ := Client.Get(Context, setting).Result()
	return settingValue
}

func getValueFromHashSet(hashSet, index, value string) (string, error) {
	hash := fmt.Sprintf("%v:%v", hashSet, index)
	query := Client.HGet(Context, hash, value).Val()
	if len(query) == 0 {
		return "", errors.New("value not found")
	}
	return query, nil
}

func getIndexFromHashSet(hashSet, value string) (string, error) {
	hash := fmt.Sprintf("%v:%v", hashSet, value)
	query := Client.HGet(Context, hash, "index").Val()
	if len(query) == 0 {
		return "", errors.New("value not found")
	}
	return query, nil
}

func GetLongURL(index string) (string, error) {
	return getValueFromHashSet("url", index, "longurl")
}

func GetShortURL(index string) (string, error) {
	return getValueFromHashSet("url", index, "shorturl")
}

func GetURLAuthor(index string) (string, error) {
	return getValueFromHashSet("url", index, "createdby")
}

func GetUserName(index string) (string, error) {
	return getValueFromHashSet("user", index, "username")
}

func GetUserHash(index string) (string, error) {
	return getValueFromHashSet("user", index, "username")
}

func GetUserNameIndex(username string) (string, error) {
	return getIndexFromHashSet("user", username)
}

func GetEmailIndex(email string) (string, error) {
	return getIndexFromHashSet("user", email)
}

func GetLongURLIndex(longURL string) (string, error) {
	return getIndexFromHashSet("url", longURL)
}

func GetShortURLIndex(shortURL string) (string, error) {
	return getIndexFromHashSet("url", shortURL)
}

func GetRandomUser() (string, error) {
	rand.Seed(time.Now().UTC().UnixNano())
	userCount := GetSetting("USER_COUNT")
	index := rand.Intn(userCount) + 1
	strIndex := strconv.Itoa(index)
	return GetUserName(strIndex)
}

func ProcessExistingURL(longURL string) {
	index, err := GetLongURLIndex(longURL)
	if err != nil {
		log.Println(err.Error())
	}
	shortURL, err := GetShortURL(index)
	if err != nil {
		log.Println(err.Error())
	}
	printExistingShortURL(shortURL)
}
