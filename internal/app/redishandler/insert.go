package redishandler

import (
	"fmt"
	"time"

	"gitlab.com/nicolebroyak1/niqurl/internal/app/randomusers"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/urlhandler"
)

func insertIntoHashSet(hashType, hashName string, values []string) {
	hash := fmt.Sprintf("%v:%v", hashType, hashName)
	Client.HSet(Context, hash, values)
}
func insertIntoSubHashSet(hashType, hashName, index string) {
	hash := fmt.Sprintf("%v:%v", hashType, hashName)
	Client.HSet(Context, hash, "index", index)
}

func insertIntoLongURL(NiqURL *urlhandler.NiqURL, index string) {
	insertIntoSubHashSet("url", NiqURL.LongURL, index)
}

func insertIntoShortURL(NiqURL *urlhandler.NiqURL, index string) {
	insertIntoSubHashSet("url", NiqURL.ShortURL, index)
}

func insertIntoURLHash(NiqURL *urlhandler.NiqURL, index string) {
	insertIntoHashSet("url", index, []string{
		"longurl", NiqURL.LongURL,
		"createdby", NiqURL.UserName,
		"shorturl", NiqURL.ShortURL,
		"index", index,
	})
}

func InsertURLData(NiqURL *urlhandler.NiqURL) {
	printInsertingURLMsg(NiqURL.String(), NiqURL.ShortURL)
	incrementURLCount()
	index := GetStringSetting("URL_COUNT")
	insertIntoShortURL(NiqURL, index) // table-like hash to query by shortURL
	insertIntoLongURL(NiqURL, index)  // table-like hash to query by longURL
	insertIntoURLHash(NiqURL, index)  // table-like hash to query by index
	insertUserWaitTime(NiqURL.UserName)
}

func InsertUsers(UsersStruct *randomusers.UsersStruct) {
	user_index := 0
	for user_index < len(UsersStruct.Results) {
		if !existsUser(UsersStruct.Results[user_index].Login.Username) {
			insertUserData(UsersStruct, user_index)
		}
		user_index++
	}
}

func insertUserData(UsersData *randomusers.UsersStruct, queryUserNum int) {
	incrementUsersCount()
	userCount := GetStringSetting("USER_COUNT")
	insertIntoUserData(UsersData, queryUserNum, userCount)
	insertUserName(UsersData, userCount, queryUserNum)
	insertEmail(UsersData, userCount, queryUserNum)
}

func insertIntoUserData(UsersData *randomusers.UsersStruct, user_index int, user_count string) {
	insertIntoHashSet("user", user_count, []string{
		"username", UsersData.Results[user_index].Login.Username,
		"email", UsersData.Results[user_index].Email,
		"firstname", UsersData.Results[user_index].Name.First,
		"lastname", UsersData.Results[user_index].Name.Last,
		"regdate", UsersData.Results[user_index].Registered.Date.String(),
		"index", user_count,
	})
}

func insertUserName(UsersData *randomusers.UsersStruct, userCount string, queryUserNum int) {
	insertIntoSubHashSet(
		"user",
		UsersData.Results[queryUserNum].Login.Username,
		userCount,
	)
}

func insertEmail(UsersData *randomusers.UsersStruct, userCount string, queryUserNum int) {
	insertIntoSubHashSet(
		"user",
		UsersData.Results[queryUserNum].Email,
		userCount,
	)
}

func insertUserWaitTime(user string) {
	waitTime := GetSetting("USER_WAIT_TIME")
	waitTime *= 1000000 // nanoseconds to miliseconds to satisft Client.Set function
	keyName := "waittime:" + user
	Client.Set(Context, keyName, true, time.Duration(int64(waitTime)))
}
