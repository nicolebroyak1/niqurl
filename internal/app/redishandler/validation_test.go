package redishandler

import (
	"testing"
	"time"
)

func TestIsValidSettingTrue(t *testing.T) {
	boolean := isValidSetting("SHORT_URL_LEN")
	if !boolean {
		t.Fatalf(`Error: %v want match for true`, boolean)
	}
}

func TestIsValidSettingFalse(t *testing.T) {
	urllen := GetStringSetting("SHORT_URL_LEN")
	Client.Del(Context, "SHORT_URL_LEN")
	boolean := isValidSetting("SHORT_URL_LEN")
	ChangeSetting("SHORT_URL_LEN", urllen)
	if boolean {
		t.Fatalf(`Error: %v want match for false`, boolean)
	}
}

func TestIsUserOnWaitTimeTrue(t *testing.T) {
	Client.Set(Context, "testuser", true, time.Duration(30000000))
	Client.Set(Context, "anothertestuser", true, time.Duration(30000000))
	boolean := IsUserOnWaitTime("anothertestuser")
	if !boolean {
		t.Fatalf(`Error: %v want match for false`, boolean)
	}
	Client.Del(Context, "testuser", "anothertestuser")
}

func TestIsUserOnWaitTimeFalse(t *testing.T) {
	Client.Set(Context, "testuser", true, time.Duration(30000000))
	boolean := IsUserOnWaitTime("anothertestuser")
	if boolean {
		t.Fatalf(`Error: %v want match for false`, boolean)
	}
	Client.Del(Context, "testuser")
}
