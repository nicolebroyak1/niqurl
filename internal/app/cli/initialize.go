package cli

import (
	"gitlab.com/nicolebroyak1/niqurl/internal/app/redishandler"

	"github.com/desertbit/grumble"
)

func initialize(a *grumble.App, flags grumble.FlagMap) error {
	redishandler.SetInvalidSettingsToDefaults()
	generateFakeUsersFlag(a, flags)
	return nil
}
