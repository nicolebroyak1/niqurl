package cli

import (
	"log"

	"github.com/desertbit/grumble"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/randomusers"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/redishandler"
)

func generateFakeUsersFlag(a *grumble.App, flags grumble.FlagMap) error {
	num := flags.Int("generate-fake-users")
	if num > 1000 || num < 1 {
		if num != 0 {
			log.Println(("Number of users has to be between 1 and 1000"))
		}
		return nil
	}
	apiSource := redishandler.GetStringSetting("DEFAULT_API_SOURCE")
	apiSource = randomusers.CreateAPISourceFromDefault(apiSource, num)
	usersStruct := randomusers.GenerateFakeUsers(apiSource, num)
	redishandler.InsertUsers(usersStruct)
	return nil
}
