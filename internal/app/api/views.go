package api

import (
	"errors"
	"net/http"
	"path"

	"github.com/gin-gonic/gin"
	"gitlab.com/nicolebroyak1/niqurl/internal/app/redishandler"
)

func RedirectURL(c *gin.Context) {
	if !redishandler.ExistsShortURL(c.Param("url")) {
		notFound(c)
		return
	}

	index, err := redishandler.GetShortURLIndex(c.Param("url"))
	if err != nil {
		notFound(c)
		return
	}

	longURL, err := redishandler.GetLongURL(index)
	if err != nil {
		notFound(c)
		return
	}

	c.Redirect(http.StatusMovedPermanently, longURL)
}

func ShowURLInfo(c *gin.Context) {
	urlInfo, err := getURLInfo(c.Param("url"))
	if err != nil {
		notFound(c)
		return
	}

	c.HTML(http.StatusOK, "inspecturl.html", urlInfo)
}

func notFound(c *gin.Context) {
	c.HTML(404, "404.html", "")
}

func getURLInfo(url string) (map[string]string, error) {
	urlInfo := map[string]string{}
	if !redishandler.ExistsShortURL(url) {
		return urlInfo, errors.New("shorturl not found")
	}
	index, err := redishandler.GetShortURLIndex(url)
	if err != nil {
		return map[string]string{}, err
	}
	urlInfo["shorturl"] = path.Join(redishandler.GetStringSetting("SERVER_PATH"), url)

	urlInfo["longurl"], err = redishandler.GetLongURL(index)
	if err != nil {
		return map[string]string{}, err
	}

	urlInfo["user"], err = redishandler.GetURLAuthor(index)
	if err != nil {
		return map[string]string{}, err
	}

	return urlInfo, nil
}
