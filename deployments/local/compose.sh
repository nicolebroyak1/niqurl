cp config/.env deployments/local/.env
cd deployments/local/
chmod +x .env && source ./.env
docker compose --env-file ./.env down
docker compose --env-file ./.env build --no-cache
docker compose --env-file ./.env up -d
rm .env
cd ../../