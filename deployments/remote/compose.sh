wget "https://gitlab.com/nicolebroyak1/niqurl/-/raw/dev/config/.env"
wget "https://gitlab.com/nicolebroyak1/niqurl/-/raw/dev/deployments/remote/docker-compose.yml"
chmod +x ./.env
source ./.env
docker compose --env-file ./.env down
docker compose --env-file ./.env build --no-cache
docker compose --env-file ./.env up -d
rm ./docker-compose.yml
rm ./.env
rm ./compose.sh
