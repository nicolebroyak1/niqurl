#### Instalation

1. Download ompose.sh file into any location in your computer
   Link: https://gitlab.com/nicolebroyak1/niqurl/raw/dev/deployments/remote/compose.sh

   If you are using linux you can use following commands:
   
```
wget https://gitlab.com/nicolebroyak1/niqurl/raw/dev/deployments/remote/compose.sh
```

1. Run compose.sh

```
. compose.sh
```

After that you should see app running in your Docker Containers list. App consists of three containers.

Note that using remote install you won't be able to config server and redis paths. To do so, please use local install.