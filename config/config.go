package config

import (
	"os"
	"strings"
)

var Configs = MakeConfigMap()

func MakeConfigMap() map[string]string {
	var Configs = make(map[string]string)
	envs, _ := os.ReadFile("/niqurl/config/.env")
	if len(envs) == 0 {
		// local dev mode
		envs, _ = os.ReadFile("./config/.env")
	}
	strenv := string(envs)
	strenvarr := strings.Split(strenv, "\n")
	for _, environ := range strenvarr {
		setting := strings.Split(environ, "=")[0]
		defaultValue := strings.Split(environ, "=")[1]
		defaultValue = strings.ReplaceAll(defaultValue, `"`, "")
		Configs[setting] = defaultValue
	}
	Configs["REDIS_PATH"] = Configs["REDIS_HOST"] + ":" + Configs["REDIS_PORT"]
	Configs["SERVER_PATH"] = Configs["SERVER_HOST"] + ":" + Configs["SERVER_PORT"]
	return Configs
}
